import logging
import requests
from string import punctuation, digits
from django.core.management.base import BaseCommand
from django.db.utils import IntegrityError, DataError
from django.core.exceptions import ValidationError
from core.models import Categories, Food

logging.basicConfig(filename="logs/updatedb.log",
                    filemode="w",
                    format='%(asctime)s - %(message)s',
                    datefmt='%d-%b-%y %H:%M:%S',
                    level=logging.INFO)


class Command(BaseCommand):
    help = "update the database via OpenFoodFacts API"

    def handle(self, *args, **options):
        categories = Categories.objects.all()

        for category in categories[:10]:
            self.stdout.write("Category: " + category.name)

            food_request_dict = self.api_food_request(category)

            self.save_food_into_db(food_request_dict)

    def api_food_request(self, category) -> dict:
        url = "https://fr.openfoodfacts.org/cgi/search.pl"

        params = {"action": "process",
                  "tagtype_0": "categories",
                  "tag_contains_0": "contains",
                  "tag_0": category.off_id,
                  "page_size": 1000,
                  "json": 1}

        request = requests.get(url, params)

        return request.json()

    def save_food_into_db(self, request_dict):
        food_list = request_dict["products"]

        for food in food_list:
            food_name = self.string_cleaner(food["product_name"])
            # Brand
            try:
                food_brand = self.string_cleaner(food["brands"].split(',')[0])
            except KeyError:
                pass
            # Nutriscore
            try:
                food_nutriscore = self.string_cleaner(food["nutrition_grades"])
            except KeyError:
                food_nutriscore = "Z"
            # Fat
            try:
                food_fat = float(food["nutriments"]["fat_100g"])
            except (KeyError, TypeError, ValueError):
                food_fat = None
            # Saturated fat
            try:
                food_saturated_fat = float(food["nutriments"][
                                               "saturated-fat_100g"])
            except (KeyError, TypeError, ValueError):
                food_saturated_fat = None
            # Sugars
            try:
                food_sugars = float(food["nutriments"]["sugars_100g"])
            except (KeyError, TypeError, ValueError):
                food_sugars = None
            # Salt
            try:
                food_salt = float(food["nutriments"]["salt_100g"])
            except (KeyError, TypeError, ValueError):
                food_salt = None
            # OpenFoodFacts url
            food_url = food["url"]
            # OpenFoodFacts image url
            try:
                food_image_url = food["image_url"]
            except KeyError:
                food_image_url = None
            # Categories
            food_categories = food["categories"].split(',')

            entry = self.update_or_create_food_entry_into_db(food_name,
                                                             food_brand,
                                                             food_nutriscore,
                                                             food_fat,
                                                             food_saturated_fat,
                                                             food_sugars,
                                                             food_salt,
                                                             food_url,
                                                             food_image_url)

            self.update_or_create_relationships_between_food_and_categories(
                    food_categories,
                    entry)

    def update_or_create_food_entry_into_db(self,
                                            name, brand, nutriscore, fat,
                                            saturated_fat, sugars, salt, url,
                                            image_url) -> object:
        try:
            entry = Food.objects.update_or_create(
                    name=name,
                    brand=brand,
                    defaults={
                        "nutriscore": nutriscore,
                        "fat": fat,
                        "saturated_fat": saturated_fat,
                        "sugars": sugars,
                        "salt": salt,
                        "url": url,
                        "image_url": image_url
                    }
            )
            if entry[1] == True:
                logging.info("Created - " + entry[0].name)
            elif entry[1] == False:
                logging.info("Updated - " + entry[0].name)

            return entry[0]

        except(IntegrityError, DataError):
            pass

    def update_or_create_relationships_between_food_and_categories(self,
                                                                   food_categories,
                                                                   entry) -> None:
        try:
            for food_category in food_categories:
                food_category = self.string_cleaner(food_category)
                category = \
                    Categories.objects.get_or_create(name=food_category)[0]
                entry.categories.add(category)
        except (ValidationError, AttributeError):
            pass

    def string_cleaner(self, my_str: str) -> str:
        for character in punctuation:
            my_str = my_str.replace(character, '')
        for character in digits:
            my_str = my_str.replace(character, '')

        my_str = my_str.title().strip()
        return my_str
