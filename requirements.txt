-i https://pypi.org/simple
certifi==2019.3.9
chardet==3.0.4
coverage==4.5.3
dj-database-url==0.5.0
django==2.2.1
gunicorn==19.9.0
idna==2.8
psycopg2-binary==2.8.2
psycopg2==2.8.2
pytz==2019.1
requests==2.21.0
selenium==3.141.0
sqlparse==0.3.0
urllib3==1.24.2
